using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float movingSpeed = 10.0f;
    public Camera firstPersonCamera;
    public GameObject projectile;
    
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }
    
    void Update()
    {
        float verticalMovement = Input.GetAxis("Vertical") * movingSpeed;
        float horizontalMovement = Input.GetAxis("Horizontal") * movingSpeed;

        verticalMovement *= Time.deltaTime;
        horizontalMovement *= Time.deltaTime;

        transform.Translate(horizontalMovement, 0, verticalMovement);

        if (Input.GetKeyDown("escape"))
        {
            Cursor.lockState = CursorLockMode.None;
        }

        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = firstPersonCamera.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
            RaycastHit hit;

            if ((Physics.Raycast(ray, out hit) == true) && hit.distance < 5 )
            {
                Debug.Log("Ray hit " + hit.collider.name);

                if (hit.collider.name.Substring(0, 3) == "Bot")
                {
                    GameObject objectHit = GameObject.Find(hit.transform.name);
                    BotController scriptObjectHit = (BotController)objectHit.GetComponent(typeof(BotController));

                    if (scriptObjectHit != null)
                    {
                        scriptObjectHit.takeDamage();
                    }
                }
            }
        }

        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = firstPersonCamera.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
            RaycastHit hit;

            GameObject proj;
            proj = Instantiate(projectile, ray.origin, transform.rotation);

            Rigidbody rb = proj.GetComponent<Rigidbody>();
            rb.AddForce(firstPersonCamera.transform.forward * 15, ForceMode.Impulse);

            Destroy(proj, 5);

            if ((Physics.Raycast(ray, out hit) == true) && hit.distance < 5)
            {
                Debug.Log("Ray hit " + hit.collider.name);

                if (hit.collider.name.Substring(0, 3) == "Bot")
                {
                    GameObject objectHit = GameObject.Find(hit.transform.name);
                    BotController scriptObjectHit = (BotController)objectHit.GetComponent(typeof(BotController));

                    if (scriptObjectHit != null)
                    {
                        scriptObjectHit.takeDamage();
                    }
                }
            }
        }
    }
}
