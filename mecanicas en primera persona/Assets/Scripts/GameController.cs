using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public GameObject player;
    public GameObject bot;
    private List<GameObject> enemyList = new List<GameObject>();
    float timeLeft;
    
    void Start()
    {
        GameStart();
    }

    void Update()
    {
        if (timeLeft == 0)
        {
            GameStart();
        }
    }
    
    void GameStart()
    {
        player.transform.position = new Vector3(0f, 0f, 0f);

        foreach (GameObject item in enemyList)
        {
            Destroy(item);
        }

        enemyList.Add(Instantiate(bot, new Vector3(5, 1f, 3f), Quaternion.identity));
        enemyList.Add(Instantiate(bot, new Vector3(3, 1f, 3f), Quaternion.identity));
        enemyList.Add(Instantiate(bot, new Vector3(1, 1f, 3f), Quaternion.identity));
        StartCoroutine(TimeStart(30));
    }

    public IEnumerator TimeStart(float timeValue = 30)
    {
        timeLeft = timeValue;
        while (timeLeft > 0)
        {
            Debug.Log("Time left: " + timeLeft + " seconds.");
            yield return new WaitForSeconds(1.0f);
            timeLeft--;
        }
    }
}
